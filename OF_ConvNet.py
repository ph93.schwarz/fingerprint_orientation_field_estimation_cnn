# -*- coding: utf-8 -*-
"""
Created on Sat Feb 15 13:56:16 2020

@author: Philipp
"""

import math
import time
import keras
import numpy as np 
import tensorflow as tf
import matplotlib.pyplot as plt
  
from PIL import Image
from operator import itemgetter
from keras.models import load_model
from keras.utils.np_utils import to_categorical 
from keras.preprocessing.image import ImageDataGenerator

import utils.preprocessing as preprocessing
import utils.visualization as visualization
import utils.neural_net as nn
import utils.callbacks as callbacks
import utils.constants as const


#############################
#                           #
#      helper functions     #
#                           #
#############################     
def getData():
    with open(const.INDEXFILE, "rt") as file:
        nrOfImg  = int(file.readline())     # first line contains nr of images
        x_train  = []
        y_train  = []
        fg_test  = []

        for line in file:                   # each line holds:
            name    = line.split()[0]       # a file name
            name    = name.split(".")[0]    # (remove file ending)
            step    = int(line.split()[1])  # a step length
            border  = int(line.split()[2])  # a border (-> top-left corner)   
            
            if(const.TRAINING):
                path_to_files = "Data"
                fg = getForground(path_to_files+const.SEP+name+const.FG)  
                fgOrig = getFGOrig(path_to_files+const.SEP+name+const.FG)
                fp = getFingerprint(path_to_files+const.SEP+name+const.BMP, 
                                    fgOrig, 
                                    border, 
                                    step)
                gt = getGroundTruth(path_to_files+const.SEP+name+const.GT, fgOrig)
                
                fg_test.append(fg)
                y_train.append(gt)
                x_train.append(fp)
    
    x_train = np.asarray(x_train)
    y_train = np.asarray(y_train)
    fg_test = np.asarray(fg_test)
    return x_train, y_train, fg_test
    
  
def getForground(filePath):
    with open(filePath, "rt") as bitmap:
        rowString, _ = bitmap.readline().split()
        rows = int(rowString)
                
        fg = [[0 for i in range(const.COLS_FG)] for j in range(const.ROWS_FG)]
        for i in range(rows):
            line = bitmap.readline()
            j = 0
            for string in line.split():
                fg[i][j] = int(string)
                j = j+1
    return np.asarray(fg)


def getFGOrig(filePath):
    with open(filePath, "rt") as bitmap:
        rowString, colString = bitmap.readline().split()
        rows = int(rowString)
        cols = int(colString)
                
        fg = [[0 for i in range(cols)] for j in range(rows)]
                
        for i in range(rows):
            line = bitmap.readline()
            j = 0
            for string in line.split():
                fg[i][j] = int(string)
                j = j+1
    return np.array(fg)
            
            
def getFingerprint(filePath, fg, border, step):
    """
    Returns the fingerprint stored in 'filePath' after applying
    histogram equalization, embedding the image in a uniform canvas and 
    finally inverting the image (=black background works better for convNets).
    """
    data = np.array(Image.open(filePath))
    data = preprocessing.whitening(data)
    norm = preprocessing.normalize(data, fg, border, step)
    #norm = preprocessing.histEqualization(norm)
    
    black = np.min(norm)
    canvas = [[black for i in range(const.COLS_FP)] for j in range(const.ROWS_FP)]
    
    for i in range(0, norm.shape[0]):         # fill canvas with data
        for j in range(0, norm.shape[1]):
            canvas[i][j] = norm[i][j]
         
    canvas = np.asarray(canvas)
    if(const.INVERT):
        canvas = np.asarray(1-canvas)
    return canvas



def getGroundTruth(filepath, fgOriginal):
    """
    Reads a byte file. Only bytes at 'valid' points are taken, which is defined 
    by the fg bits. Moreover, every second byte is ommited as it contains 
    information we don't need. The ground truth
    """
    file = open(filepath,'rb')
    file.read(32)
    fgList = fgOriginal.flatten().tolist()
    oriImageCat = [
            [to_categorical(0, const.NUMBER_OF_CLASSES) 
            for i in range(const.COLS_FG)] 
            for j in range(const.ROWS_FG)
    ]
    ctr = 0
    for i in range(fgOriginal.shape[0]):
        for j in range(fgOriginal.shape[1]):
            if(fgList[ctr] == 1):
                oriImageCat[i][j] = to_categorical(getOrientation(file), const.NUMBER_OF_CLASSES)
                file.read(1)
            else:
                oriImageCat[i][j] = to_categorical(0, const.NUMBER_OF_CLASSES)
                file.read(2) 
            ctr +=1
            
    return np.asarray(oriImageCat)   


def getOrientation(f):
    """
    Returns a value between [0, 255] 
    which represents an angle of an orientation [0, 180) as a unit of 180/256
    """
    return int.from_bytes(f.read(1), byteorder='little')
   

def augmentTrainData(x_train, y_train):
    images  = x_train.tolist()
    labels  = y_train.tolist()
    
    for k in range(x_train.shape[0]):
        if(const.ADD_NOISE):
            images.append(np.asarray(preprocessing.addSaltAndPepper(x_train[k], 0.4)))
            labels.append(y_train[k])
            images.append(np.asarray(preprocessing.addSaltAndPepper(x_train[k], 0.6)))
            labels.append(y_train[k])
        if(const.FLIP_LR):
            images.append(np.fliplr(x_train[k]))
            labels.append(np.fliplr(y_train[k]))
        if(const.FLIP_UD):
            images.append(np.flipud(x_train[k]))
            labels.append(np.flipud(y_train[k]))
    '''      
    for k in range(x_train.shape[0]):
        if(FLIP_LR):
            images.append(np.fliplr(x_train[k]))
            labels.append(np.fliplr(y_train[k]))
        if(FLIP_UD):
            images.append(np.flipud(x_train[k]))
            labels.append(np.flipud(y_train[k]))
        if(FLIP_LR_AND_UD):
            images.append(np.flipud(np.fliplr(x_train[k])))
            labels.append(np.flipud(np.fliplr(y_train[k])))
    '''      
    return np.asarray(images), np.asarray(labels)
   

def train_model(cnn, x_train, y_train, x_val, y_val, i):
    x_train = x_train.reshape(x_train.shape[0], 
                              const.ROWS_FP, 
                              const.COLS_FP, 1)
    y_train = y_train.reshape(y_train.shape[0], 
                              const.ROWS_FG, 
                              const.COLS_FG, 
                              const.NUMBER_OF_CLASSES)
    x_val = x_val.reshape(x_val.shape[0], 
                          const.ROWS_FP, 
                          const.COLS_FP, 1)
    y_val = y_val.reshape(y_val.shape[0], 
                          const.ROWS_FG, 
                          const.COLS_FG, 
                          const.NUMBER_OF_CLASSES)

    train_generator = ImageDataGenerator(
        #brightness_range = [0.6, 1.2],
        #zca_whitening = True,
        data_format = 'channels_last')
    
    val_generator = ImageDataGenerator(
        #brightness_range = [0.6, 1.2],
        #zca_whitening = True,
        data_format = 'channels_last')
        
    train_generator.fit(x_train)
    val_generator.fit(x_val)
    
    es = callbacks.early_stopping(patience=4)
    
    print(cnn.summary())
    with tf.device('/gpu:0'):
        hist = cnn.fit_generator(
                train_generator.flow(x_train, y_train, batch_size=const.batch_size),
                steps_per_epoch = len(y_train),
                epochs = const.NUMBER_OF_EPOCHS,
                verbose = 2,
                shuffle = const.shuffle,                      
                callbacks = [es], 
                validation_data = val_generator.flow(x_val, y_val, batch_size=const.batch_size),
                validation_steps = len(y_val))

    cnn.save('{}_{}.h5'.format(const.model_path, str(i)))
    #lr_finder.plot_loss()  
    return hist


def foregroundAcc():
     
    def accuracy(y_true, y_pred): 
        zero = tf.constant(0, 'int32')
        zero_oh = tf.one_hot(zero, 256)
        mask = 1-zero_oh
        y_pred_masked = y_pred*mask
        y_true_masked = y_true*mask
        return keras.metrics.categorical_accuracy(y_true_masked, y_pred_masked)
        
    return accuracy


def foregroundCCE():
    
    def loss(y_true, y_pred):
        zero = tf.constant(0, 'int32')
        zero_oh = tf.one_hot(zero, 256)
        mask = 1-zero_oh
        y_pred_masked = y_pred*mask
        y_true_masked = y_true*mask
        return keras.losses.categorical_crossentropy(y_true_masked, y_pred_masked)

    return loss


def getGenerators():
    train_generator = ImageDataGenerator(
        data_format = 'channels_last')
    
    val_generator = ImageDataGenerator(
        data_format = 'channels_last')
    return train_generator, val_generator
    

def test_model(x_test, y_test, fg_test, i):
    x_test = x_test.reshape(x_test.shape[0], const.ROWS_FP, const.COLS_FP, 1)

    def accuracy(y_true, y_pred): 
        zero = tf.constant(0, 'int32')
        zero_oh = tf.one_hot(zero, 256)
        mask = 1-zero_oh
        y_pred_masked = y_pred*mask
        y_true_masked = y_true*mask
        return keras.metrics.categorical_accuracy(y_true_masked, y_pred_masked)

    def loss(y_true, y_pred):
        zero = tf.constant(0, 'int32')
        zero_oh = tf.one_hot(zero, 256)
        mask = 1-zero_oh
        y_pred_masked = y_pred*mask
        y_true_masked = y_true*mask
        return keras.losses.categorical_crossentropy(y_true_masked, y_pred_masked)


    cnn = load_model('{}_{}.h5'.format(const.model_path, str(i)), 
                     custom_objects={'loss':loss, 'accuracy': accuracy})
    
    predictions = cnn.predict(x_test)
    top16_pred = np.zeros((x_test.shape[0], const.ROWS_FG, const.COLS_FG))
    ground_truth = np.zeros((x_test.shape[0], const.ROWS_FG, const.COLS_FG))
    idx = np.arange(const.NUMBER_OF_CLASSES)

    for i in range(x_test.shape[0]):
        for row in range(const.ROWS_FG):
            for col in range(const.COLS_FG):
                ground_truth[i][row][col] = np.argmax(y_test[i][row][col])
                class_pred_lst = list(zip(idx, predictions[i][row][col]))
                #sort lst by prediction (highest pred first)
                class_pred_lst = sorted(class_pred_lst, key=itemgetter(1))
                #take the best TOP_K entries 
                tupleList = class_pred_lst[-const.TOP_K:]
                
                # save predictions in additional list for scaling
                probaList = []
                for t in tupleList:
                    probaList.append(t[1])
                
                #scale probabilities (sum should add up to 1)
                probaList = scaleProba(probaList)
                j = 0
                x = 0
                y = 0
                for t in tupleList:
                    deg = convertToDegree(t[0])
                    rad = math.radians(deg)
                    sin = math.sin(2*rad)
                    cos = math.cos(2*rad)
                    #weighted average of scaled probabilities
                    x += cos*probaList[j]
                    y += sin*probaList[j]
                    j+=1
                    
                degree = math.degrees(math.atan2(y, x)/2)
                if(degree < 0):
                    degree = degree+180
                
                # get closest class for this orientation (in units of 180/256)
                top16_pred[i][row][col] = int(degree*256/180)

        '''
        #plot orientation images    
        topk = [[0 for i in range(COLS_FG)] for j in range(ROWS_FG)]
        topk_no_fg = [[0 for i in range(COLS_FG)] for j in range(ROWS_FG)]
        for row in range(ROWS_FG):
            for col in range(COLS_FG):
                topk[row][col] = top16_pred[i][row][col]
                if(fg_test[i][row][col] == 1):
                    topk_no_fg[row][col] = top16_pred[i][row][col]
        
        #plotImage(topk)
        #plotImage(topk_no_fg)
        #plotImage(ground_truth[i])
        '''
    rmse = rootMeanSquareError(ground_truth, top16_pred, fg_test)
    print("Average rmse top{} = {}".format(const.TOP_K, rmse))
    


def scaleProba(probaList): 
    s = sum(probaList)
    probaList = [p * 1/s for p in probaList] 
    return probaList  
            

def rootMeanSquareError(y, y_pred, fg):
    s = 0
    rmse = 0
    deviation_lst = []
    for i in range(y.shape[0]):
        for row in range(0, np.asarray(fg).shape[1]):
            for col in range(0, np.asarray(fg).shape[2]):
                if(fg[i][row][col] == 1):
                    groundTruth = y[i][row][col]
                    prediction  = y_pred[i][row][col]
                    gt_degree   = convertToDegree(groundTruth)
                    pred_degree = convertToDegree(prediction)
                    
                    diff = abs(gt_degree - pred_degree)
                    # handle jumps between 180 and 0 degree
                    if diff > 90:
                        diff = 180-diff
                    deviation_lst.append(diff)
                    s += (diff)**2
                    #if(pred_degree > 1):
                    #print("gt = {}° \t\t pred = {}° \t\t diff = {}°".format(gt_degree, pred_degree, diff))
        rmse += math.sqrt(s/fg[i].flatten().tolist().count(1))
        s = 0
    
    plt.hist(deviation_lst)
    plt.xlabel('Deviation (in degree). {} images. {} orientations in total'.format(y.shape[0], len(deviation_lst)))
    plt.ylabel('Counts')
    plt.show()
    return rmse/y.shape[0]


def convertToDegree(a):
    return a*180/256


def get_fold_indices():
    fold_indices = []
    img_per_fold = int(const.NR_OF_IMAGES/const.FOLDS)
    indices  = np.random.permutation(const.NR_OF_IMAGES)
    
    #validation only
    fold_indices.append((indices[img_per_fold:], indices[0:img_per_fold]))
    for i in range(1, const.FOLDS):
        val_idx = indices[i*img_per_fold:(i+1)*img_per_fold]
        train_idx = [x for x in range(const.NR_OF_IMAGES) if (x not in val_idx)]
        fold_indices.append((np.asarray(train_idx), val_idx))
 
    
    #validation and testing
    '''
    fold_indices.append((indices[2*img_per_fold:], indices[0:img_per_fold], indices[img_per_fold:2*img_per_fold]))
    for i in range(1, FOLDS):
        val_idx = indices[i*img_per_fold:(i+1)*img_per_fold]
        if(i != FOLDS-1):
            test_idx = indices[(i+1)*img_per_fold:(i+2)*img_per_fold]
        else:
            test_idx = indices[0:img_per_fold]
        train_idx = [x for x in range(NR_OF_IMAGES) if (x not in val_idx and x not in test_idx)]
        fold_indices.append((np.asarray(train_idx), val_idx, test_idx))
        
    '''
    return fold_indices

#############################
#                           #
#         code area         #
#                           #
#############################    
# load data and preprocess it
x, y, fg = getData()
#create foldes for k-fold cross validation (k can be changed in constants.py)
fold_indices = get_fold_indices()
# layer names are not specified. names change after every run.
conv_nr = const.nr

if const.CROSSVAL:
    for i in range(const.FOLDS):
        #train_indices, val_indices, test_idx = fold_indices[i]
        train_indices, val_indices = fold_indices[i]
        print("train indices = ", train_indices)
        print("val indices = ", val_indices)
        #print("test_idx = ", test_idx)
        print("Training on fold " + str(i+1) + "/" + str(const.FOLDS) +"...")
            
        #x_train, x_val, x_test = x[train_indices], x[val_indices], x[test_idx]
        #y_train, y_val, y_test = y[train_indices], y[val_indices], y[test_idx]
        fg_val = fg[val_indices]
            
        x_train, x_val = x[train_indices], x[val_indices]
        y_train, y_val = y[train_indices], y[val_indices]
        
        x_train, y_train = augmentTrainData(x_train, y_train)
                
        # Clear model, and (re)create it for next run
        cnn = None
        cnn = nn.create_model()
        cnn.compile(loss=foregroundCCE(), 
                optimizer=nn.getOptimizer(),
                metrics=[foregroundAcc()])
            
        print("Training new iteration on " + str(x_train.shape[0]) + 
              " training samples, " + str(x_val.shape[0]) + 
              " validation samples, this may take a while...")
        history = train_model(cnn, x_train, y_train, x_val, y_val, i)
        visualization.plotLearningCurve(history, 0)
        
        '''
        print(cnn.summary())
        plot_conv_weights(cnn, 'conv2d_' + str(conv_nr))
        plot_conv_weights(cnn, 'conv2d_' + str(conv_nr+1))
        plot_conv_weights(cnn, 'conv2d_' + str(conv_nr+2))
        plot_conv_weights(cnn, 'conv2d_' + str(conv_nr+3))
        plot_conv_weights(cnn, 'conv2d_' + str(conv_nr+4))
        plot_conv_weights(cnn, 'conv2d_' + str(conv_nr+5))
        
        conv_nr += 10
        '''
        
        start = time.time()
        test_model(x_val, y_val, fg_val, i)
        end = time.time() 
        print('Finished testing in {} seconds.'.format(end-start))


