# -*- coding: utf-8 -*-
"""
Created on Sat Feb 15 14:18:17 2020

@author: Philipp
"""

from keras import initializers, regularizers, optimizers
from keras.models import Model
from keras.layers import Conv2D, MaxPooling2D, Input, BatchNormalization
from keras.layers.core import Dropout
from keras.layers.advanced_activations import LeakyReLU, PReLU, ReLU, Softmax

import utils.constants as const


# layers
def relu(x):
    return ReLU()(x)


def leaky_relu(x):
    return LeakyReLU(alpha=const.alpha)(x)


def prelu(x):
    return PReLU()(x)


def conv(x, nr_of_filter, kernel_size, weight_decay):
    return Conv2D(nr_of_filter, 
                  kernel_size=(kernel_size, kernel_size), 
                  padding='same',
                  kernel_regularizer=regularizers.l2(weight_decay)
                  )(x)


def softmax(x):
    return Softmax()(x)


def max_pool(x, pool_size):
    return MaxPooling2D(pool_size=(pool_size, pool_size))(x)


def batch_norm(x):
    return BatchNormalization(momentum=const.momentum)(x)


def dropout(x, drop_rate):
    return Dropout(drop_rate)(x)


def block_init(input, initializer, nr_of_filter, kernel_size):
    x = Conv2D(nr_of_filter, kernel_size=(kernel_size, kernel_size), padding='same',
               kernel_initializer = initializer,
               kernel_regularizer=regularizers.l2(const.weight_decay)
               )(input)
    
    x = leaky_relu(x)
    x = max_pool(x, 2)
    x = batch_norm(x)
    return x
    

def create_model():
    input = Input(shape=(const.ROWS_FP, const.COLS_FP, 1))
    #he_uniform = initializers.he_uniform(seed=constants.SEED)
    he_normal = initializers.he_normal(seed=const.SEED)
    
    m = block_init(input, he_normal, 25, 7)
    m = conv(m, 32, 5, const.weight_decay)
    m = pool_lrelu_batch(m)
    m = conv(m, 32, 5, const.weight_decay)
    m = pool_lrelu_batch(m)
    
    m = conv(m, 48, 13, const.weight_decay)
    m = lrelu_batch(m)
    m = conv(m, 48, 11, const.weight_decay)
    m = lrelu_batch(m)
    m = conv(m, 48, 11, const.weight_decay)
    m = lrelu_batch(m)
     
    m = conv(m, 32, 1, const.weight_decay)
    m = lrelu_batch(m)
    m = dropout(m, 0.35)
    
    m = conv(m, 48, 1, const.weight_decay)
    m = lrelu_batch(m)
    m = dropout(m, 0.35)
    
    m = conv(m, const.NUMBER_OF_CLASSES, 1, const.weight_decay)
    m = softmax(m)
    return Model(input, m)


def pool_lrelu_batch(x):
    x = leaky_relu(x)
    x = max_pool(x, 2)
    x = batch_norm(x)
    return x


def lrelu_batch(x):
    x = leaky_relu(x)
    x = batch_norm(x)
    return x


def getOptimizer():
    return optimizers.Nadam(lr=const.initial_lr,
                            beta_1=0.9,
                            beta_2=0.999,
                            epsilon=None,
                            schedule_decay=const.decay)
    
    '''
    return optimizers.SGD(lr=const.initial_lr, 
                             momentum=const.momentum, 
                             decay=const.decay,
                             nesterov=True)
    
    return optimizers.Nadam(lr=const.initial_lr,
                            beta_1=0.9,
                            beta_2=0.999,
                            epsilon=None,
                            schedule_decay=const.decay)
    
    return optimizers.Adadelta(lr=const.initial_lr, 
                               rho=0.95,
                               epsilon=None,
                               decay=const.decay)
    
    return optimizers.Adamax(lr=const.initial_lr, 
                             beta_1=0.9,
                             beta_2=0.999,
                             epsilon=None,
                             decay=const.decay)
    '''
    
