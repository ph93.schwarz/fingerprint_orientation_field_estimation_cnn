# Fingerprint_Orientation_Field_Estimation_CNN

Includes code for training a convolutional neural network to perform the task of fingerprint orientation estimation and code for extracting the orientation field of fingerprints in a way that is compatible with FVC-onGoing (https://biolab.csr.unibo.it/FVCOnGoing/UI/Form/Home.aspx)

"""
License
-------

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
"""